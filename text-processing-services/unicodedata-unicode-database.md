# unicodedata



این ماژول به شما کمک می کند تا به دیتابیس کاراکتر های یونیکد دسترسی پیدا کنید که تمام خصوصیات کاراکتر ها را برای کاراکتر ههای یونیکد تعریف کرده است. تمام اطلاعات از دیتابیس  [UCD version 12.1.0](http://www.unicode.org/Public/12.1.0/ucd) گردآوری شده است.

 این ماژول از همان اسم ها و نمادهایی استفاده می کند که توسط استاندارد یونیکد استاندارد شماره 44 ، ["](https://www.unicode.org/reports/tr44/)[دیتابیس کاراکتر های یونیکد"](https://www.unicode.org/reports/tr44/)  تعریف شده است. این ماژول توابع زیر را تعریف می کند:

####  `unicodedata.lookup`\(_name_\)

دنبال کاراکتر با نام داده شده می گردد و اگر پیدا شد متناظر آن را برمی گرداند ولی اگر پیدا نشد  `KeyError` دریافت می کنید.

تغییر در نسخه 3.3 : پشتیبانی از نامهای مستعار \(name aliases\) و دنباله های نامگذاری شده اضافه شده است.

#### `unicodedata.name`\(_chr_\[, _default_\]\)

نام اختصاص داده شده به کاراکتر chr را به صورت یک رشته برمی گرداند.اگر نامی تعریف نشده باشد ، پیش فرض بازگردانده می شود ، یا در صورتی که چیزی به آن ندهید ، ValueError دریافت می شود.

####  `unicodedata.decimal`\(_chr_\[, _default_\]\)

مقدار اعشار اختصاص داده شده به کاراکتر chr را به صورت یک عدد صحیح \(int\) برمی گرداند. اگر مقداری تعریف شده نباشد ، پیشفرض بر گردانده می شود و اگر پیشفرضی تعریف نشده باشد،   `ValueError` در یافت می کنید.

####  `unicodedata.digit`\(_chr_\[, _default_\]\)

مقدار رقمی اختصاص داده شده به کاراکتر chr را به صورت عدد صحیح برمی گرداند. اگر مقداری تعریف شده نباشد ، پیشفرض بر گردانده می شود و اگر پیشفرضی تعریف نشده باشد،   `ValueError` در یافت می کنید.

####  `unicodedata.numeric`\(_chr_\[, _default_\]\)

مقدار عددی اختصاص داده شده به کاراکتر CHR را به صورت float \(اعشاری\) برمی گرداند. اگر مقداری تعریف شده نباشد ، پیشفرض بر گردانده می شود و اگر پیشفرضی تعریف نشده باشد،   `ValueError` در یافت می کنید.

####  `unicodedata.category`\(_chr_\)

دسته کلی اختصاص داده شده به کاراکتر chr را به صورت یک رشته برمی گرداند.

####  `unicodedata.bidirectional`\(_chr_\)

کلاس دو طرفه اختصاص داده شده به کاراکتر را به صورت رشته برمی گرداند. اگر چیزی تعریف نشده باشد رشته ای خالی بر می گرداند.

####  `unicodedata.east_asian_width`\(_chr_\)

طول شرق آسیایی ای  را که به کاراکتر اختصاص داده شده را به صورت یک رشته برمی گرداند.

####  `unicodedata.mirrored`\(_chr_\)

خصوصیت آینه ای اختصاص داده شده به کاراکتر را به صورت عدد صحیح بر می گرداند. اگر کاراکتر در متن 2 طرفه “mirrored” شناخته شده باشد 1 را برمی گرداند در غیر اینصورت 0.

#### `unicodedata.decomposition`\(_chr_\)

نقشه نقشه تجزیه کاراکتر اختصاص داده شده به کاراکتر chr را به عنوان رشته برمی گرداند. در صورت عدم وجود چنین نقشه ای ، رشته ای خالی برگردانده می شود.

####  `unicodedata.normalize`\(_form_, _unistr_\)





