# ^^ rlcompleter

ماژول rlcompleter با تکمیل شناسه های معتبر پایتون و کلمات کلیدی ، یک ماژول کامل کننده برای readline است.

هنگامی که این ماژول بر روی پلت فرم Unix با ماژول readline فعال import می شود، یک نمونه از کلاس Completer خودکار ساخته می شود و متد

 `complete()`

به عنوان کامل کننده ی readline استفاده می شود.

مثال:

```text
>>> import rlcompleter
>>> import readline
>>> readline.parse_and_bind("tab: complete")
>>> readline. <TAB PRESSED>
readline.__doc__          readline.get_line_buffer(  readline.read_init_file(
readline.__file__         readline.insert_text(      readline.set_completer(
readline.__name__         readline.parse_and_bind(
>>> readline.
```

ماژول rlcompleter برای استفاده با حالت تعاملی پایتون طراحی شده است. مگر اینکه پایتون در حالت [`-s`](https://docs.python.org/3/using/cmdline.html#id3) اجرا شود. ماژول به طور خودکار import و تنظیم می شود \( [Readline configuration](https://docs.python.org/3/library/site.html#rlcompleter-config) را بخوانید\). 

### اشیاء Completer

اشیاء Completer متد های زیر را دارند:

 `Completer.complete`\(_text_, _state_\)

گزینه ها ای برای تکمیل متن برمی گرداند. اگر متنی به آن دهیم که نقطه نداشته باشد، آن متن با نام های تعریف شده در \_\_main\_\_ و داخلی \(builtins\) و کلمات کلیدی \(تعریف شده با ماژول  `keyword`\).

اگر به آن یک dotted name بدهیم سعی می کند تا آن را بدون هیچ عوارض جانبی آشکار \(توابع ارزیابی نمی شوند ، اما می تواند که تماسی با تابع \_\_gather\_\_ برقرار کند\) از اول تا آخر ارزیابی کند و همخوان \(یکی\) ها را پیدا کند و بقیه را از طریق تابع dir  . همه ی خطا های ممکن کچ \( try & catch\) شده و تابع چیزی بر نمی گرداند.







